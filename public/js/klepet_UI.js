/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  };
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

//SLIKE 
function elementSlika(sporocilo){
  var besede = sporocilo.split(' ');
  besede.push('<br/>');
  for(var st=0;st<sporocilo.split(" ").length;st++){
    if(besede[st].toLowerCase().endsWith(".") || besede[st].toLowerCase().endsWith(",") || besede[st].toLowerCase().endsWith("!") || besede[st].toLowerCase().endsWith("?") || besede[st].toLowerCase().endsWith("-") || besede[st].toLowerCase().endsWith(")") || besede[st].toLowerCase().endsWith("(")){
      besede[st]=besede[st].slice(0, -1);
      if(besede[st].toLowerCase().startsWith("http") && (besede[st].toLowerCase().endsWith(".jpg") || besede[st].toLowerCase().endsWith(".gif") || besede[st].toLowerCase().endsWith(".png") )){
        $('#sporocila').append($('<img>',{id:'img',src: besede[st], style:'margin-left: 20px; width:200px'})).append('<br/>');
      }
    }
    else if(besede[st].toLowerCase().startsWith("http") && (besede[st].toLowerCase().endsWith(".jpg") || besede[st].toLowerCase().endsWith(".gif") || besede[st].toLowerCase().endsWith(".png") )){
        $('#sporocila').append($('<img>',{id:'img',src: besede[st], style:'margin-left: 20px; width:200px'})).append('<br/>');
      }
  }
  
  sporocilo=besede.join(' ');
  return divElementEnostavniTekst(sporocilo);
}

/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  var krc = "&#9756";
  if(sporocilo.localeCompare(krc)){
    return divElementHtmlTekst(sporocilo);
  }

  else if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  }else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    elementSlika(sporocilo);
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));

  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}

///////////////////////HASH TABLE///////////////////////////////////////

function HashTable(){
  this.size = 0;
  this.dataStore = {};
}

HashTable.prototype.put = function(key, value){
    this.dataStore[key]=value;
    this.size +=1;
    return this.dataStore;
};

HashTable.prototype.contains = function(key){
  if(this.dataStore.hasOwnProperty(key)){
    return true;
  }
  else{
    return false;
    
  }
};


HashTable.prototype.remove = function(key){
  if(this.dataStore.hasOwnProperty(key)){
    delete this.dataStore[key];
    this.size -=1;
    return this.dataStore;
  }
};

HashTable.prototype.size = function(){
  return this.size;
};


HashTable.prototype.returnValue =function(key){
  return this.dataStore[key];
};

///////////////////////////////////////////////////
var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";
var ht = new HashTable();
var ht1 = new HashTable();


// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    
    var sporocilo;
    if(ht.contains("1")){
      ht.remove("1");
      ht.put("1",rezultat.vzdevek);
    }
    else{ht.put("1",rezultat.vzdevek);}
    
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var str=sporocilo.besedilo.replace(sporocilo.vzdevek,"");
    var novElement="";

    if(ht.contains(sporocilo.vzdevek)){
        var novLook=' ('+sporocilo.vzdevek+')';
        var nadimek=ht.returnValue(sporocilo.vzdevek);
        var nov=nadimek+novLook
        var beseda=nov+str;
        novElement = divElementEnostavniTekst(beseda);
    }
    
    else{
      novElement=divElementEnostavniTekst(sporocilo.besedilo);
    }
    
    if(ht.returnValue(sporocilo.prejsnjiVzdevek)!=undefined){
        var temp=ht.returnValue(sporocilo.prejsnjiVzdevek);
        ht.remove(sporocilo.prejsnjiVzdevek);
        ht.put(sporocilo.vzdevek,temp);
    }
   
    
    
    $('#sporocila').append(novElement);
    elementSlika(sporocilo.besedilo);
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      //Če se nahaja v Hash tabeli
      if(ht.contains(uporabniki[i])){
         var nadimek=ht.returnValue(uporabniki[i]);
         var nov=nadimek+' ('+uporabniki[i]+')';
         $('#seznam-uporabnikov').append(divElementEnostavniTekst(nov));
         if(ht1.contains(nov)){
           ht1.remove(nov);
         }
         ht1.put(nov,uporabniki[i]);
      }
      else{$('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));}
    }
    
    //Klik na ime uporabnika v seznam-->krc
    $('#seznam-uporabnikov div').click(function() {
      var ime=$(this).text();;
      if(ht1.contains(ime)){
        ime=ht1.returnValue(ime);
      }
      var ukaz="/zasebno "+ '"'+ime +'" '+ '"'+"&#9756"+'"';
      var sistemskoSporocilo = klepetApp.procesirajUkaz(ukaz);
      if (sistemskoSporocilo) {
        $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
      }
      $('#poslji-sporocilo').val('');
    });
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});