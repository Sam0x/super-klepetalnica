// V tej datoteki je opredeljen objekt Klepet, 
// ki nam olajša obvladovanje funkcionalnosti uporabnika


var Klepet = function(socket) {
  this.socket = socket;
};


/**
 * Pošiljanje sporočila od uporabnika na strežnik z uporabo 
 * WebSocket tehnologije po socketu 'sporocilo'
 * 
 * @param kanal ime kanala, na katerega želimo poslati sporočilo
 * @param besedilo sporočilo, ki ga želimo posredovati
 */
Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};


/**
 * Uporabnik želi spremeniti kanal, kjer se zahteva posreduje na strežnik 
 * z uporabo WebSocket tehnologije po socketu 'pridruzitevZahteva'
 * 
 * @param kanal ime kanala, kateremu se želimo pridružiti
 */
Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};


/**
 * Procesiranje ukaza, ki ga uporabnik vnese v vnosno polje, kjer je potrebno
 * najprej izluščiti za kateri ukaz gre, nato pa prebrati še parametre ukaza
 * in zahtevati izvajanje ustrezne funkcionalnosti
 * 
 * @param ukaz besedilo, ki ga uporabnik vnese v vnosni obrazec na spletu
 */
Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  // Izlušči ukaz iz vnosnega besedila
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;
  
  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      // Sproži spremembo kanala
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      // Zahtevaj spremembo vzdevka
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var besedilo = besede.join(' ');
      var parametri = besedilo.split('\"');
      if (parametri) {
        if(ht.contains(parametri[1])){
          this.socket.emit('sporocilo', {vzdevek: parametri[1], besedilo: parametri[3]});
          sporocilo = '(zasebno za ' +  ht.returnValue(parametri[1]) +' ('+ parametri[1] +')' +'): ' + parametri[3];
        }
        
        else{
          this.socket.emit('sporocilo', {vzdevek: parametri[1], besedilo: parametri[3]});
          sporocilo = '(zasebno za ' + parametri[1] + '): ' + parametri[3];
          
        }
      } else {
        sporocilo = 'Neznan ukaz';
      }
      break;
     
     case 'barva':
        besede.shift();
        var ozadje = besede.join(' ');
        var element = document.querySelector("#sporocila");
        element.style.backgroundColor = ozadje;
        break; 

    
    case 'preimenuj':
      besede.shift();
      var besedilo1 = besede.join(' ');
      var parametri1 = besedilo1.split('\"');
      var nadimek = parametri1[3];
      var praviVzdevek = parametri1[1];
      
      if(ht.returnValue("1").localeCompare(praviVzdevek)==0){
        sporocilo = 'Sam si ne mores dati vzdevka!'; 
        }
      else if(ht.contains(praviVzdevek)){
        ht.remove(praviVzdevek);
        ht.put(praviVzdevek,nadimek);
        }
      else{
        ht.put(praviVzdevek,nadimek);
      }
      
      break;


    default:
      // Vrni napako, če pride do neznanega ukaza
      sporocilo = 'Neznan ukaz.';
      break;
  }

  return sporocilo;
};
